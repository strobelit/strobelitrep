//
//  ViewController.swift
//  V2Tors
//
//  Created by Thom Jonsson Persson on 2018-04-12.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var names = [String]()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thecell", for: indexPath) as!FancyCollectionViewCell
        
        cell.backgroundColor = UIColor.red
        
        cell.nameLabel.text = names[indexPath.item]
        
        cell.numberLabel.text = String(indexPath.item+1)
        
        return cell
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        names.append("Göran")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width/2, height: self.view.frame.width/2)
        
    }

}

