//
//  FancyCollectionViewCell.swift
//  V2Tors
//
//  Created by Thom Jonsson Persson on 2018-04-12.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class FancyCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
}
