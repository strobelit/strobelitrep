//
//  ChildViewController.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-05-03.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class ChildViewController: UIViewController {

    public var isVisible = false
    
    public var viewControllerDelegate : ViewController!

}
