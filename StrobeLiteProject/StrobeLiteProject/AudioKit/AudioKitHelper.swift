//
//  AudioKitHelper.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-03-15.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit
import AudioKit

class AudioKitHelper {

    public func startFrequencyTracker(callbackFn: @escaping  (_ frequency: Double, _ amplitude: Double) -> ()) {
        let mic = AKMicrophone()
        let filter = AKHighPassFilter(mic, cutoffFrequency: Constants.HOPSIZE, resonance: 0)
        let tracker = AKFrequencyTracker(filter)
        let silent = AKBooster(tracker, gain: 0)

//        var currentSampleRate = 0
//        var frequencies = Frequencies.init()

        let periodicFunction = AKPeriodicFunction(frequency: Constants.SAMPLINGRATE) {
            callbackFn(tracker.frequency, tracker.amplitude)

//            frequencies.addFrequency(frequency: tracker.frequency, amplitude: tracker.amplitude, sensitivity: Double(SharedData.sharedInstance.sensitivity))
//            currentSampleRate += 1
//
//            if (currentSampleRate > Int(Constants.FLASHLIGHT_INTERVAL * Constants.SAMPLINGRATE)) {
//                callbackFn(frequencies)
//
//                currentSampleRate = 0;
//                frequencies = Frequencies.init()
//            }
        }
        
        do {
            AudioKit.output = silent
            try AudioKit.start(withPeriodicFunctions: periodicFunction)
            print("AudioKit works!")
        }
        catch {
            print("AudioKit didn't work")
        }
        
        periodicFunction.start()
    }
    
    public func stopFrequencyTracker() {
        print("AudioKit stopped!")
        try! AudioKit.stop()
    }
    
}
