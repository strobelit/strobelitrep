//
//  ViewController.swift
//  StrobeLiteProject
//
//  Created by Thom Jonsson Persson on 2018-03-07.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit
import AudioKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var settings2Button: UIButton!
    @IBOutlet weak var microphonePermissionText: UILabel!
    @IBOutlet weak var coveringView: UIView!
    
    lazy var microphonePermissionVC: MicrophonePermissionViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MicrophonePermission") as! MicrophonePermissionViewController
        
        viewController.viewControllerDelegate = self
                
        return viewController
    }()
    
    let audioKitHelper = AudioKitHelper()
    let strobelight = StrobeLight()
    var numberOfLoops = 0
    var numberOfSilentLoops = 0
    var frequencies : Frequencies!
    var microphonePermissionDenied = true
    @IBOutlet weak var strobeLitesLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sharedData = SharedData.sharedInstance

        frequencies = Frequencies.init()
        
        toggleTextLabel(show: false)
        
        styleView(view: microphonePermissionVC.view)
        styleView(view: settingsView)
        
        settingsView.transform = CGAffineTransform(translationX: -1500, y: 0)

        sharedData.sensitivity = UserDefaults.standard.float(forKey: Constants.userDefaultKeys.SENSITIVITY)

        if (sharedData.sensitivity == 0.0) {
            sharedData.sensitivity = Constants.DEFAULT_SENSITIVITY
        }

        let recognizer = UITapGestureRecognizer(target: self, action:#selector(tapMainViewController(recognizer:)))

        recognizer.delegate = self
        coveringView.addGestureRecognizer(recognizer)
        
        AKSettings.session.requestRecordPermission { (allowed) in
            if (allowed) {
                print("Previously allowed use of microphone")
                self.startFrequencyTracker()
                
                self.microphonePermissionDenied = false
            } else {
                print("Denied use of microphone")
                self.tapMainViewController(recognizer: recognizer)
                self.strobeLitesLogo.image = UIImage(named: "Mask")
                
                self.microphonePermissionDenied = true
            }
        }
    }
    
    private func styleView(view: UIView) {
        view.layer.borderWidth = 1.5
        view.layer.borderColor = UIColor.darkGray.cgColor
        view.layer.cornerRadius = 5;
        view.layer.masksToBounds = true;
    }

    public func startFrequencyTracker() {
        self.audioKitHelper.startFrequencyTracker(callbackFn: self.getFrequencyFromTracker)
    }
    
    deinit {
        print("Device stopped!")
        audioKitHelper.stopFrequencyTracker()
    }
    
    private func getFrequencyFromTracker(frequency : Double, amplitude : Double) {
        frequencies.addFrequency(frequency: frequency, amplitude: amplitude)
        numberOfLoops += 1
        
        if (frequencies.maxAmplitude * SharedData.sharedInstance.sensitivity <= Constants.MIN_AMPLITUDE) {
            numberOfSilentLoops += 1

            let seconds = numberOfSilentLoops / Constants.SAMPLINGRATE

            if (seconds > 5 && microphonePermissionText.text != "Can't register any sound.") {
                showTextLabel(text: "Can't register any sound.")
            } else if (seconds > 2 && (microphonePermissionText.isHidden || microphonePermissionText.text != "Can't register any sound.")) {
                let dots = seconds > 3 ? ( seconds > 4 ? "..." : ".." ) : "."
                showTextLabel(text: "Searching for sounds" + dots)
            }
        } else {
            numberOfSilentLoops = 0
            toggleTextLabel(show: false)
        }
        
        if (numberOfLoops > Int(Constants.FLASHLIGHT_INTERVAL * Constants.SAMPLINGRATE)) {
            print("------")
            print("sub base        ", frequencies.subBase.amplitude)
            print("base            ", frequencies.base.amplitude)
            print("midrange        ", frequencies.midrange.amplitude)
            print("midrange, upper ", frequencies.midrangeUpper.amplitude)
            print("presence        ", frequencies.presence.amplitude)
            print("brilliance      ", frequencies.brilliance.amplitude)
            print(":: maxAmplitude ", frequencies.maxAmplitude)

            numberOfLoops = 0

            let opacity = strobelight.toggleTorch(frequencies: frequencies, sensitivity: SharedData.sharedInstance.sensitivity)
            
            if (opacity != coveringView.backgroundColor?.cgColor.alpha) {
                coveringView.backgroundColor = UIColor.white.withAlphaComponent(opacity)
            }
            
            frequencies = Frequencies.init()    // reset frequencies to zero
        }
    }
    
    public func showTextLabel(text: String) {
        toggleTextLabel(show: true)
        microphonePermissionText.text = text
    }
    
    public func toggleTextLabel(show: Bool) {
        microphonePermissionText.text = "";
        microphonePermissionText.isHidden = !show
    }
    
    public func addViewController(asChildViewController viewController: ChildViewController) {
        let maxSize = CGPoint(x: 300, y: 500)
        addViewController(asChildViewController: viewController, maxSize: maxSize)
    }
    
    public func addViewController(asChildViewController viewController: ChildViewController, maxSize: CGPoint) {
        let viewWidth  = (view.bounds.width  < maxSize.x) ? view.bounds.width  : maxSize.x
        let viewHeight = (view.bounds.height < maxSize.y) ? view.bounds.height : maxSize.y
        
        let viewXPosition = (view.bounds.width - viewWidth) / 2
        let viewYPosition = (view.bounds.height - viewHeight) / 2
        
        let viewFrame = CGRect(x: viewXPosition, y: viewYPosition, width: viewWidth, height: viewHeight)
        
        viewController.isVisible = true
        
        addViewController(asChildViewController: viewController, viewFrame: viewFrame)
    }
    
    public func addViewController(asChildViewController viewController: ChildViewController, viewFrame: CGRect) {
        addChildViewController(viewController)
        
        view.addSubview(viewController.view)
        
        viewController.view.frame = viewFrame
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        viewController.didMove(toParentViewController: self)
    }

    public func removeViewController(asChildViewController viewController: ChildViewController) {
        viewController.isVisible = false

        viewController.willMove(toParentViewController: nil)
        
        viewController.view.removeFromSuperview()
        
        viewController.removeFromParentViewController()
    }
    
    @IBAction func settingsOpen(_ sender: Any) {
        removeViewController(asChildViewController: microphonePermissionVC)
        
        UIView.animate(withDuration: Constants.ANIMATION_SPEED, animations: {
            self.settingsView.transform = CGAffineTransform(translationX: 0, y: 0)
        })
        settingsButton.isHidden = true
        settings2Button.isHidden = false
    }
    
    @IBAction func settingsClose(_ sender: Any) {
        UIView.animate(withDuration: Constants.ANIMATION_SPEED, animations: {
            self.settingsView.transform = CGAffineTransform(translationX: -1500, y: 0)
        })
        
        settingsButton.isHidden = false
        settings2Button.isHidden = true
    }
    
}

