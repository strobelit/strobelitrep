//
//  Flashlight.swift
//  StrobeLiteProject
//
//  Created by Malmö Yrkeshögskola on 2018-03-26.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class StrobeLight {
    var counter: Int = 0
    var timer: Timer
    var isStrobing: Bool
    var isLightOn: Bool
    var frequency: Double
    var start = DispatchTime.now()
    var end = DispatchTime.now()
    var active: Bool
    var torchOutput: Float = 0.0
    
    init (){
        self.counter = 0
        self.timer = Timer()
        self.isStrobing = false
        self.isLightOn = false
        self.frequency = 200
        self.active = false
        self.torchOutput = 0.0
    }
    
    func setTorchOutput(highest: Float)  {
        torchOutput = highest
    }

    // Turns light on or off
    func toggleTorch(frequencies: Frequencies, sensitivity: Float) -> CGFloat {
        let base = Float(frequencies.base.amplitude) * sensitivity
        let presence = Float(frequencies.presence.amplitude) * sensitivity
        let minAmplitude = Float(Constants.MIN_AMPLITUDE)
        let amplitude = (base > presence) ? base : presence

        let turnOnTorch = (amplitude > self.torchOutput * Constants.AMPLITUDE_RANGE
                    && amplitude > minAmplitude)
        
        self.torchOutput = amplitude
        
//        print("amplitude:", amplitude)
//        print("base:", base)
//        print("presence:", presence)
//        print("minAmplitude:", minAmplitude)
//        print("on:", on)
//        print("sensitivity:", sensitivity)
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return returnTorchLevel(turnOnTorch: amplitude > minAmplitude, amplitude: amplitude) }
        if device.hasTorch {
            if device.isTorchAvailable{
                do {
                    try device.lockForConfiguration()
                    if turnOnTorch == true {
                        do {
                            try device.setTorchModeOn(level: amplitude)
//                            print(":: Light turned on!")
                        } catch { print("Could not set torch level") }
//                        device.torchMode = .on
                        
                    } else {
                        device.torchMode = .off
//                        print(":: Light turned off!")
                        
                    }
                    device.unlockForConfiguration()
                } catch {
                    print("Torch could not be used")
                }
            } else {
                print( "torch unavailable")
            }
        } else {
            print("torch unavailable")
        }

        return returnTorchLevel(turnOnTorch: turnOnTorch, amplitude: amplitude)
    }
    
    private func returnTorchLevel(turnOnTorch: Bool, amplitude: Float) -> CGFloat {
        return turnOnTorch ? CGFloat(amplitude) : 0.0;
    }
    
}
