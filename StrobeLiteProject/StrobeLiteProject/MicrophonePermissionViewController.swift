//
//  MicrophonePermissionViewController.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-05-01.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit
import AVFoundation

class MicrophonePermissionViewController: ChildViewController {
    
    @IBOutlet weak var denyPermissionBtn: UIButton!
    
    @IBAction func microphonePermission(_ sender: UIButton) {
        let recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { (allowed) in
                DispatchQueue.main.async {
                    if allowed {
                        print("User allowed microphone")
                        self.viewControllerDelegate.toggleTextLabel(show: false)
                        self.viewControllerDelegate.startFrequencyTracker()
                        self.viewControllerDelegate.strobeLitesLogo.image = UIImage(named: "StrobeLiteIcon")
                        self.viewControllerDelegate.microphonePermissionDenied = false
                        self.closeModal()
                    } else {
                        self.viewControllerDelegate.showTextLabel(text: Constants.Text.PERMISSION_DENIED)
                        print("User denied microphone")
                    }
                }
            }
        } catch {
            self.viewControllerDelegate.showTextLabel(text: Constants.Text.PERMISSION_DENIED + " You need to change this yourself in app settings")
            print("User denied microphone previously")
        }
    }
    
    @IBAction func closeModal(_ sender: UIButton) {
        self.viewControllerDelegate.showTextLabel(text: Constants.Text.PERMISSION_DENIED)
        closeModal()
    }
    
    func closeModal() {
        viewControllerDelegate.removeViewController(asChildViewController: viewControllerDelegate.microphonePermissionVC)
    }
}
