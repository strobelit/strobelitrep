//
//  Frequence.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-03-19.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import Foundation

class Frequency {
    var count: Double
    var amplitude: Double
    
    init() {
        self.count = 0.0
        self.amplitude = 0.0
    }
    
    init(count: Double, amplitude: Double) {
        self.count = count
        self.amplitude = amplitude
    }
}
