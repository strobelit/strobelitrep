//
//  Frequencies.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-03-19.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class Frequencies {

    var subBase:        Frequency
    var base:           Frequency
    var midrange:       Frequency
    var midrangeUpper:  Frequency
    var presence:       Frequency
    var brilliance:     Frequency
    var maxAmplitude:   Double
    
    init() {
        self.subBase =       Frequency.init()
        self.base =          Frequency.init()
        self.midrange =      Frequency.init()
        self.midrangeUpper = Frequency.init()
        self.presence =      Frequency.init()
        self.brilliance =    Frequency.init()
        self.maxAmplitude =  0.0
    }
    
    func calculateAverageValue(amplitude: Double, frequency: Frequency) -> Frequency {
        let previousAmplitudes = frequency.amplitude * frequency.count
        let newCount = frequency.count + 1.0
        let averageAmplitude = (previousAmplitudes + amplitude) / newCount

        return Frequency.init(count: newCount, amplitude: averageAmplitude)
    }
    
    public func addFrequency(frequency: Double, amplitude: Double) {
        if (amplitude > self.maxAmplitude) {
            self.maxAmplitude = amplitude
        }

        switch frequency {
            case Constants.Frequency.HUMAN_HEARING..<Constants.Frequency.SUB_BASS:
                self.subBase = self.calculateAverageValue(amplitude: amplitude, frequency: self.subBase)
            
            case Constants.Frequency.SUB_BASS..<Constants.Frequency.BASS:
                self.base = self.calculateAverageValue(amplitude: amplitude, frequency: self.base)

            case Constants.Frequency.BASS..<Constants.Frequency.MIDRANGE:
                self.midrange = self.calculateAverageValue(amplitude: amplitude, frequency: self.midrange)

            case Constants.Frequency.MIDRANGE..<Constants.Frequency.MIDRANGE_UPPER:
                self.midrangeUpper = self.calculateAverageValue(amplitude: amplitude, frequency: self.midrangeUpper)

            case Constants.Frequency.MIDRANGE_UPPER..<Constants.Frequency.PRESENCE:
                self.presence = self.calculateAverageValue(amplitude: amplitude, frequency: self.presence)

            case Constants.Frequency.PRESENCE..<Constants.Frequency.BRILLIANCE:
                self.brilliance = self.calculateAverageValue(amplitude: amplitude, frequency: self.brilliance)

            default:
                print("Over 20k Hz")
        }
    }
    
}
