//
//  ForViewController.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-05-03.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

extension ViewController : UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
    
    @objc func tapMainViewController(recognizer: UIGestureRecognizer) {
        if (settingsButton.isHidden) {
            UIView.animate(withDuration: Constants.ANIMATION_SPEED, animations: {
                self.settingsView.transform = CGAffineTransform(translationX: -1500, y: 0)
            })
            settingsButton.isHidden = false
            settings2Button.isHidden = true
        }
        else if (microphonePermissionDenied) {
            if (microphonePermissionVC.isVisible) {
                if (self.microphonePermissionDenied) {
                  self.showTextLabel(text: Constants.Text.PERMISSION_DENIED)
                }
                
                removeViewController(asChildViewController: microphonePermissionVC)
            } else {
                addViewController(asChildViewController: microphonePermissionVC, maxSize: Constants.MICROPHONE_MODAL_SIZE)
            }
        }
    }
}
