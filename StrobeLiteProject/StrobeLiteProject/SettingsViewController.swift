//
//  settingsViewController.swift
//  StrobeLiteProject
//
//  Created by Thom Jonsson Persson on 2018-03-29.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var sensitivitySlider: UISlider!
    
    @IBAction func sensitivitySlider(_ sender: Any) {
        let sensitivity = sensitivitySlider.value
        
        print("Sensitivity: ", sensitivity)
        
        SharedData.sharedInstance.sensitivity = sensitivity
        
        UserDefaults.standard.set(sensitivity, forKey: Constants.userDefaultKeys.SENSITIVITY)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let savedSliderSensitivity = UserDefaults.standard.float(forKey: Constants.userDefaultKeys.SENSITIVITY)
        
        if (savedSliderSensitivity > 0) {
            sensitivitySlider.value = UserDefaults.standard.float(forKey: Constants.userDefaultKeys.SENSITIVITY)
        }
    }
}
