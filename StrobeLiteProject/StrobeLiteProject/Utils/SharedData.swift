//
//  SharedData.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-04-09.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

class SharedData {
    
    var sensitivity : Float = 0.55
    
    static let sharedInstance = SharedData()
    
}
