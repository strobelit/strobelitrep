//
//  Dimensions.swift
//  StrobeLiteProject
//
//  Created by Rickard on 2018-03-12.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

struct Constants {
    static let SAMPLINGRATE:        Double = 50     // 1/SAMPLINGRATE per seconds
    static let HOPSIZE:             Double = 20     // Hz - lowest frequency
//    static let PEAKCOUNT:           Int = 20000     // Hz - highest frequency
    static let MIN_AMPLITUDE:       Double = 0.5
    static let FLASHLIGHT_INTERVAL: Double = 1/10   // seconds
    static let AMPLITUDE_RANGE:     Float  = 0.9    // percentage
    static let DEFAULT_SENSITIVITY: Float  = 1.05
    static let ANIMATION_SPEED:     Double = 0.3
    static let MICROPHONE_MODAL_SIZE       = CGPoint(x: 300, y: 300)

    struct Frequency {
        static let HUMAN_HEARING:   Double = 20     // Hz
        static let SUB_BASS:        Double = 60
        static let BASS:            Double = 500
        static let MIDRANGE:        Double = 2000
        static let MIDRANGE_UPPER:  Double = 4000
        static let PRESENCE:        Double = 6000
        static let BRILLIANCE:      Double = 20000
    }
    
    struct Text {
        static let PERMISSION_DENIED = "You must give the app permission to gain access to the microphone in order for it to work."
    }
    
    struct userDefaultKeys {
        static let SENSITIVITY = "sensitivity"
    }
}


