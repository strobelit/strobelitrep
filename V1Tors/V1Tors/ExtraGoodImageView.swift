//
//  ExtraGoodImageView.swift
//  V1Tors
//
//  Created by Thom Jonsson Persson on 2018-04-05.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

class ExtraGoodImageView: UIImageView {
    
    @IBInspectable var howMuchCorner: CGFloat = 0 {
        didSet {
            layer.cornerRadius = howMuchCorner
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
