//
//  SpecialAlertView.swift
//  V1Tors
//
//  Created by Thom Jonsson Persson on 2018-04-05.
//  Copyright © 2018 Thom Jonsson Persson. All rights reserved.
//

import UIKit

@IBDesignable
class SpecialAlertView: UIView {

    @IBInspectable var makeItTransparent: Bool = false {
        didSet {
            if(makeItTransparent == true)
            {
                self.backgroundColor = UIColor.red
                
                //headerLabel.textColor = UIColor.white
                //textLabel.textColor = UIColor.white
            }else
            {
                self.backgroundColor = UIColor.gray
                //headerLabel.textColor = UIColor.black
                //textLabel.textColor = UIColor.black
            }
        }
    }
    
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var textLabel: UILabel!
    
}
